<?php

use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tbl_customer_details')->insert([
          'surname' => 'Abel',
          'firstname' => 'Adam',
          'secondname' => 'Alex',
          'id_number' => '11111111',
          'phone_number' => '722111111',
          'status' => 'registered',
      ]);
      DB::table('tbl_customer_details')->insert([
          'surname' => 'Boston',
          'firstname' => 'Bran',
          'secondname' => 'Black',
          'id_number' => '22222222',
          'phone_number' => '722222222',
          'status' => 'registered',
      ]);
      DB::table('tbl_customer_details')->insert([
          'surname' => 'Chris',
          'firstname' => 'Cramer',
          'secondname' => 'Chiloba',
          'id_number' => '33333333',
          'phone_number' => '722333333',
          'status' => 'unregistered',
      ]);
      DB::table('tbl_customer_details')->insert([
          'surname' => 'Danson',
          'firstname' => 'David',
          'secondname' => 'Degea',
          'id_number' => '44444444',
          'phone_number' => '722444444',
          'status' => 'unregistered',
      ]);
      DB::table('tbl_customer_details')->insert([
          'surname' => 'Emily',
          'firstname' => 'Eve',
          'secondname' => 'Eva',
          'id_number' => '55555555',
          'phone_number' => '722555555',
          'status' => 'unregistered',
      ]);

    }
}
