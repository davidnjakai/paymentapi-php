<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessAccount extends Model
{
    protected $table = 'tbl_business_accounts';
}
