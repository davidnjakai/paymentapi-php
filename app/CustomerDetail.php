<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDetail extends Model
{
    protected $table = 'tbl_customer_details';

    public function mobile_wallet()
    {
      return $this->hasOne(MobileWallet::class, 'phone_number');
    }
}
