<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/c2ctransfer', 'ApiController@c2ctransfer');
Route::post('/transaction_details', 'ApiController@transaction_details');
Route::post('/transaction_history', 'ApiController@transaction_history');
