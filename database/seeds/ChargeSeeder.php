<?php

use Illuminate\Database\Seeder;

class ChargeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tbl_charges')->insert([
          'min' => 0,
          'max' => 1000,
          'withdraw_charge' => 0,
          'send_to_unregistered' => 0,
          'send_to_registered' => 0,
      ]);
      DB::table('tbl_charges')->insert([
          'min' => 1001,
          'max' => 10000,
          'withdraw_charge' => 112,
          'send_to_unregistered' => 205,
          'send_to_registered' => 87,
      ]);
      DB::table('tbl_charges')->insert([
          'min' => 10001,
          'max' => 30000,
          'withdraw_charge' => 180,
          'send_to_unregistered' => 288,
          'send_to_registered' => 102,
      ]);
      DB::table('tbl_charges')->insert([
          'min' => 30001,
          'max' => 50000,
          'withdraw_charge' => 270,
          'send_to_unregistered' => null,
          'send_to_registered' => 105,
      ]);
      DB::table('tbl_charges')->insert([
          'min' => 50001,
          'max' => 70000,
          'withdraw_charge' => 300,
          'send_to_unregistered' => null,
          'send_to_registered' => 105,
      ]);
    }
}
