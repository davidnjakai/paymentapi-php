<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubTransaction extends Model
{
    protected $table = 'tbl_sub_transactions';

    public function main_transaction()
    {
      return $this->belongsTo('App\MainTransaction', 'receipt');
    }
}
