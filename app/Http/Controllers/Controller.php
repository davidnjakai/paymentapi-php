<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function phone_format_valid($phone)
    {
      $phone_pattern_1 = '/^2547[0-9]{8}/';
      $phone_pattern_2 = '/^07[0-9]{8}/';
      $phone_pattern_3 = '/^7[0-9]{8}/';

      return preg_match($phone_pattern_1, $phone) || preg_match($phone_pattern_2, $phone) || preg_match($phone_pattern_3, $phone);
    }
}
