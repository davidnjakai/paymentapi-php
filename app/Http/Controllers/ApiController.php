<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\MobileWallet;
use App\CustomerDetail;
use App\MainTransaction;
use App\SubTransaction;
use App\BusinessAccount;
use App\Charge;

use Illuminate\Http\Request;

class ApiController extends Controller
{
      public function c2ctransfer(Request $request)
      {
        $rules = array(
           'partya' => 'required|string|max:12',
           'partyb' => 'required|string|max:12',
           'amount' => 'required|integer|max:70000',
           'pin' => 'required|string',
           );

       $validator = Validator::make($request->all(), $rules);

       if ($validator->fails()) {
         $messages = $validator->messages();
         return $messages;
       }

       $partya = substr($request->partya, -9);
       $partyb = substr($request->partyb, -9);

       if (!Controller::phone_format_valid($partya)) {
         return "Party a phone format incorrect";
       }

       if (!Controller::phone_format_valid($partyb)) {
         return "Party b phone format incorrect";
       }

       $amount = $request->amount;
       $pin = $request->pin;

       $mobile_wallet_a = MobileWallet::where('phone_number', $partya)->first();
       $mobile_wallet_b = MobileWallet::where('phone_number', $partyb)->first();
       $business_account = BusinessAccount::first();

       if (!Hash::check($pin, $mobile_wallet_a->pin)){
         return "Invalid pin";
       }

       $customer_b = CustomerDetail::where('phone_number', $partyb)->first();

       if ($customer_b->status == 'unregistered' && $amount > 30000) {
         return "You can send maximum of 30000 for unregistered numbers.";
       }

       $main_transaction = New MainTransaction;

       $receipt = str_random(10);

       while (MainTransaction::where('receipt', $receipt)->count() > 0) {
         $receipt = str_random(10);
       }

       $charge = Charge::where('min', '<=', $amount)->where('max', '>=', $amount)->first();

       if ($customer_b->status == 'unregistered') {
         $charge_amount = $charge->send_to_unregistered;
       }
       else {
         $charge_amount = $charge->send_to_registered;
       }

       $total_charge = $amount + $charge_amount;

       if ($mobile_wallet_a->account_balance < $total_charge) {
         return "Insufficient balance for that transaction";
       }

       $mobile_wallet_a->account_balance -= $total_charge;
       $mobile_wallet_a->save();

       $mobile_wallet_b->account_balance += $amount;
       $mobile_wallet_b->save();

       $business_account->account_balance += $charge_amount;
       $business_account->save();

       $main_transaction->receipt = $receipt;
       $main_transaction->request_id = $receipt;
       $main_transaction->partya = $partya;
       $main_transaction->partyb = $partyb;
       $main_transaction->amount = $amount;
       $main_transaction->status = 'success';
       $main_transaction->charge = $charge_amount;

       $main_transaction->save();

       $sub_transaction_a = new SubTransaction;
       $sub_transaction_a->request_id = $receipt;
       $sub_transaction_a->receipt = $receipt;
       $sub_transaction_a->phone_number = $partya;
       $sub_transaction_a->amount = $amount;
       $sub_transaction_a->amount_type = 'transaction_amount';
       $sub_transaction_a->transaction_type = 'debit';
       $sub_transaction_a->save();

       $sub_transaction_b = new SubTransaction;
       $sub_transaction_b->request_id = $receipt;
       $sub_transaction_b->receipt = $receipt;
       $sub_transaction_b->phone_number = $partyb;
       $sub_transaction_b->amount = $amount;
       $sub_transaction_b->amount_type = 'transaction_amount';
       $sub_transaction_b->transaction_type = 'credit';
       $sub_transaction_b->save();

       $sub_transaction_c = new SubTransaction;
       $sub_transaction_c->request_id = $receipt;
       $sub_transaction_c->receipt = $receipt;
       $sub_transaction_c->phone_number = $business_account->account_no;
       $sub_transaction_c->amount = $charge_amount;
       $sub_transaction_c->amount_type = 'charge';
       $sub_transaction_c->transaction_type = 'credit';
       $sub_transaction_c->save();

       return $main_transaction->toJson();
      }

      public function transaction_details(Request $request)
      {
        $receipt = $request->receipt;

        $main_transaction = MainTransaction::where('receipt', $receipt)->first();
        return $main_transaction->toJson();
      }

      public function transaction_history(Request $request)
      {
        try {
          $rules = array(
             'start_date' => 'required|string',
             'end_date' => 'required|string',
             'phone' => 'required|string|max:12',
             );

         $validator = Validator::make($request->all(), $rules);

         if ($validator->fails()) {
           $messages = $validator->messages();
           return $messages;
         }

         if (!Controller::phone_format_valid($request->phone)) {
           return "Incorrect phone format";
         }
         $phone = substr($request->phone, -9);

          $start_date = date_create_from_format('YmdHis', $request->start_date);
          $end_date = date_create_from_format('YmdHis', $request->end_date);

          $transactions = MainTransaction::where(function($query) use ($phone){
            $query->where('partya', $phone)->orWhere('partyb', $phone);
          })->whereBetween('created_at', [$start_date, $end_date])->get();

          return $transactions->toJson();
        } catch (\Exception $e) {
          return "dates must be in the format: dmYHis(e.g. 31122019235959)";
        }

      }
}
