<?php

use Illuminate\Database\Seeder;

class BusinessAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // TODO: Add maximum limit for account_balance column
    public function run()
    {
      DB::table('tbl_business_accounts')->insert([
          'account_no' => '722000000',
          'account_name' => 'XYZ',
          'status' => 'active',
          'account_balance' => 60000,
      ]);
    }
}
