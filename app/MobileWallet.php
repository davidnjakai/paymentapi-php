<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileWallet extends Model
{
    protected $table = 'tbl_mobile_wallets';

    public function customer()
    {
      return $this->belongsTo(CustomerDetail::class, 'phone_number');
    }
}
