<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_main_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('request_id');
            $table->text('receipt');
            $table->text('partya');
            $table->text('partyb');
            $table->integer('amount');
            $table->text('status');
            $table->integer('charge');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_main_transactions');
    }
}
