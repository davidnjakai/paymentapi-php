<?php

use Illuminate\Database\Seeder;

class MobileWalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tbl_mobile_wallets')->insert([
          'surname' => 'Abel',
          'firstname' => 'Adam',
          'secondname' => 'Alex',
          'phone_number' => '722111111',
          'pin' => Hash::make('1234'),
          'account_balance' => 100000,
      ]);
      DB::table('tbl_mobile_wallets')->insert([
          'surname' => 'Boston',
          'firstname' => 'Bran',
          'secondname' => 'Black',
          'phone_number' => '722222222',
          'pin' => Hash::make('1234'),
          'account_balance' => 100000,
      ]);
      DB::table('tbl_mobile_wallets')->insert([
          'surname' => 'Chris',
          'firstname' => 'Cramer',
          'secondname' => 'Chiloba',
          'phone_number' => '722333333',
          'pin' => Hash::make('1234'),
          'account_balance' => 100000,
      ]);
      DB::table('tbl_mobile_wallets')->insert([
          'surname' => 'Danson',
          'firstname' => 'David',
          'secondname' => 'Degea',
          'phone_number' => '722444444',
          'pin' => Hash::make('1234'),
          'account_balance' => 100000,
      ]);
      DB::table('tbl_mobile_wallets')->insert([
          'surname' => 'Emily',
          'firstname' => 'Eve',
          'secondname' => 'Eva',
          'phone_number' => '722555555',
          'pin' => Hash::make('1234'),
          'account_balance' => 100000,
      ]);

    }
}
