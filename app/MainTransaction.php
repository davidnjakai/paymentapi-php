<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainTransaction extends Model
{
    protected $table = 'tbl_main_transactions';

    public function subtransactions()
    {
      return $this->hasMany('App\SubTransaction', 'receipt');
    }
}
